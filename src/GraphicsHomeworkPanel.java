/**
 * File: GraphicsHomeworkPanel
 * Author: Michelle John
 * Date: 04 November 2018
 * Purpose: Homework 1: Java2D Scene
 */

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.util.Random;

/**
 * The panel upon which the scene is drawn. Extends {@link JPanel}.
 */
class GraphicsHomeworkPanel extends JPanel {

  /**
   * Constructor. Creates the panel and sets the background color.
   */
  GraphicsHomeworkPanel() {
    super();
    setBackground(Color.CYAN);
  }

  @Override
  public void paintComponent(Graphics graphicElement) {
    super.paintComponent(graphicElement);
    drawSun(graphicElement);
    drawCloud(graphicElement, 50F, 150F);
    drawCloud(graphicElement, 200F, 125F);
    drawCloud(graphicElement, 350F, 75F);
    drawMountain(graphicElement);
    drawFootHills(graphicElement);
    drawBird(graphicElement);
    drawTree(graphicElement, 50, 400);
    drawTree(graphicElement, 225, 400);
    drawTree(graphicElement, 350, 375);
  }

  /**
   * Creates and draws a sun graphic.
   *
   * @param graphicElement the graphic element to use
   */
  private void drawSun(Graphics graphicElement) {
    Graphics2D graphic2DElement = (Graphics2D) graphicElement;
    Ellipse2D circle = new Ellipse2D.Double(75, 75, 75, 75);
    graphic2DElement.setColor(Color.YELLOW);
    graphic2DElement.fill(circle);
  }

  /**
   * Creates and draws a cloud.
   *
   * @param graphicElement the graphic element to use
   * @param x the x-coordinate of the lower left point of the cloud
   * @param y the y-coordinate of the lower left point of the cloud
   */
  private void drawCloud(Graphics graphicElement, float x, float y) {
    Graphics2D graphics2DElement = (Graphics2D) graphicElement;
    GeneralPath cloud = new GeneralPath();
    cloud.moveTo(x, y);
    cloud.lineTo(x + 100, y);
    cloud.quadTo(x + 100, y - 25, x + 75, y - 25);
    cloud.quadTo(x + 75, y - 50, x + 50, y - 50);
    cloud.quadTo(x + 25, y - 50, x + 25, y - 25);
    cloud.quadTo(x, y - 25, x, y);
    graphics2DElement.setColor(Color.WHITE);
    graphics2DElement.fill(cloud);
  }

  /**
   * Creates and draws the foothills.
   *
   * @param graphicElement the graphics element to use
   */
  private void drawFootHills(Graphics graphicElement) {
    Graphics2D graphics2DElement = (Graphics2D) graphicElement;
    GeneralPath hill = new GeneralPath();
    hill.moveTo(0F, 500F);
    hill.lineTo(50F, 375F);
    hill.lineTo(125F, 475F);
    hill.lineTo(200F, 400F);
    hill.lineTo(275F, 490F);
    hill.lineTo(325F, 425F);
    hill.lineTo(400F, 480F);
    hill.lineTo(500F, 400F);
    hill.lineTo(500F, 500F);
    hill.closePath();
    graphics2DElement.setColor(Color.LIGHT_GRAY);
    graphics2DElement.fill(hill);
  }

  /**
   * Creates and draws a mountain.
   *
   * @param graphicElement the graphics element to use
   */
  private void drawMountain(Graphics graphicElement) {
    Graphics2D graphics2DElement = (Graphics2D) graphicElement;
    GeneralPath mountain = new GeneralPath();
    mountain.moveTo(0F, 500F);
    mountain.lineTo(0F, 400F);
    mountain.lineTo(125F, 300F);
    mountain.lineTo(250F, 400F);
    mountain.lineTo(325F, 300F);
    mountain.lineTo(400F, 425F);
    mountain.lineTo(500F, 290F);
    mountain.lineTo(500F, 500F);
    mountain.closePath();
    graphics2DElement.setColor(Color.GRAY);
    graphics2DElement.fill(mountain);
  }

  /**
   * Creates and draws a bird.
   *
   * @param graphicElement the graphics element to use
   */
  private void drawBird(Graphics graphicElement) {
    Graphics2D graphics2DElement = (Graphics2D) graphicElement;
    GeneralPath bird = new GeneralPath();
    bird.moveTo(275F, 250F);
    bird.quadTo(300F, 225F, 325F, 250F);
    bird.quadTo(350F, 225F, 375F, 250F);
    graphics2DElement.setStroke(new BasicStroke(5F));
    graphics2DElement.setColor(Color.BLACK);
    graphics2DElement.draw(bird);
  }

  /**
   * Creates and draws a tree.
   *
   * @param graphicElement the graphics element to use
   * @param x the x-coordinate of the upper right corner of the trunk
   * @param y the y-coordinate of the upper right corner of the trunk
   */
  private void drawTree(Graphics graphicElement, int x, int y) {
    Graphics2D graphics2DElement = (Graphics2D) graphicElement;
    Rectangle2D trunk = new Rectangle2D.Double(x, y, 20, 500 - y);
    graphics2DElement.setColor(new Color(165, 42, 42)); // brown
    graphics2DElement.fill(trunk);
    Random random = new Random();
    for (int i = 0; i < 20; i++) {
      int xPos = (x - 50) + random.nextInt(75);
      int yPos = (y + 25) - random.nextInt(75);
      drawLeaves(graphicElement, xPos, yPos);
    }
  }

  /**
   * Draws the leaves of a tree.
   *
   * @param graphicElement the graphics element to use
   * @param x the x-coordinate of the upper right corner of the rectangle that encompasses the leaves
   * @param y the y-coordinate of the upper right corner of the rectangle that encompasses the leaves
   */
  private void drawLeaves(Graphics graphicElement, float x, float y) {
    Graphics2D graphics2DElement = (Graphics2D) graphicElement;
    Random random = new Random();
    int leafSize = random.nextInt(20) + 40;
    int greenColor = random.nextInt(20) + 110;
    Ellipse2D leaves = new Ellipse2D.Double(x, y, leafSize, leafSize);
    graphics2DElement.setColor(new Color(0, greenColor, 0));
    graphics2DElement.fill(leaves);
  }
}
