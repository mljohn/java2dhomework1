/**
 * File: GraphicsHomework
 * Author: Michelle John
 * Date: 04 November 2018
 * Purpose: Homework 1: Java2D Scene
 */

import javax.swing.*;

/**
 * Main class and entry point into the program.
 */
public class GraphicsHomework {

  /**
   * Entry point into the program.
   *
   * @param args the arguments to use when this program starts
   */
  public static void main(String[] args) {
    new GraphicsHomework();

  }

  /**
   * Constructor. Creates the frame and the panel with the Java2D scene.
   */
  private GraphicsHomework() {
    JFrame frame = new JFrame("Homework 1 Graphic");
    frame.setSize(500, 539);
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    GraphicsHomeworkPanel panel = new GraphicsHomeworkPanel();

    frame.add(panel);
    frame.setVisible(true);
  }
}
